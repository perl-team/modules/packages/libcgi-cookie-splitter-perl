libcgi-cookie-splitter-perl (0.05-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove AGOSTINI Yves from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 10 Jun 2022 00:46:09 +0100

libcgi-cookie-splitter-perl (0.05-2) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Damyan Ivanov ]
  * Reverse the order of the alternative dependency on Test::Simple
  * Claim conformance with policy 3.9.7

 -- Damyan Ivanov <dmn@debian.org>  Sun, 21 Feb 2016 19:08:16 +0000

libcgi-cookie-splitter-perl (0.05-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 0.05
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl
  * Dependencies: remove libmodule-build-tiny-perl and add
    libnamespace-clean-perl

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 17 Aug 2015 10:10:57 -0300

libcgi-cookie-splitter-perl (0.04-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update build dependencies.
  * Install new CONTRIBUTING file.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 19:08:15 +0100

libcgi-cookie-splitter-perl (0.03-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop patch, applied upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 14 Feb 2014 20:59:13 +0100

libcgi-cookie-splitter-perl (0.02-3) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Add patch to fix hash randomisation issues.
    Thanks to Shlomi Fish for the patch.
    Closes: #711430
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 20 Jan 2014 23:04:48 +0100

libcgi-cookie-splitter-perl (0.02-2) unstable; urgency=low

  [ Frank Lichtenheld ]
  * Fix typo in description.  Closes: #491150

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Florian Schlichting ]
  * Reword long description so that the bug can finally be closed.
  * Refresh debian/copyright.
  * Bump Standards-Version to 3.9.2 (simplify perl dependency).
  * Bump debhelper compatibility to 8 / source format "3.0 (quilt)"
    with minimal debian/rules.
  * Add myself to Uploaders.

  [ gregor herrmann ]
  * debian/control: make short description a noun phrase.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Mon, 12 Sep 2011 19:25:36 +0000

libcgi-cookie-splitter-perl (0.02-1) unstable; urgency=low

  * Initial Release (Closes: #476932)

 -- AGOSTINI Yves <agostini@univ-metz.fr>  Wed, 16 Apr 2008 11:25:29 +0200
